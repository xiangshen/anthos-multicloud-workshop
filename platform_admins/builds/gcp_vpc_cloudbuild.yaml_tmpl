# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

---
timeout: 3600s
logsBucket: "gs://${PROJECT_ID}/logs/cloudbuild"
steps:
  - name: "gcr.io/kaniko-project/executor:v0.15.0"
    id: "build-installer-image"
    args:
      - --destination=gcr.io/${PROJECT_ID}/platform-installer
      - --cache=true
      - --cache-ttl=12h
  - name: gcr.io/${PROJECT_ID}/platform-installer
    id: "prod-gcp-vpc"
    dir: infrastructure/prod/gcp/vpc
    waitFor: ['build-installer-image']
    entrypoint: "bash"
    secretEnv: ['AWS_ACCESS_KEY_ID', 'AWS_SECRET_ACCESS_KEY']
    args:
      - "-xe"
      - "-c"
      - |
        terraform init
        terraform plan -out terraform.tfplan
        terraform apply -input=false terraform.tfplan
  - name: gcr.io/${PROJECT_ID}/platform-installer
    id: "stage-gcp-vpc"
    dir: infrastructure/stage/gcp/vpc
    waitFor: ['build-installer-image']
    entrypoint: "bash"
    args:
      - "-xe"
      - "-c"
      - |
        terraform init
        terraform plan -out terraform.tfplan
        terraform apply -input=false terraform.tfplan
  - name: gcr.io/${PROJECT_ID}/platform-installer
    id: "dev-gcp-vpc"
    dir: infrastructure/dev/gcp/vpc
    waitFor: ['build-installer-image']
    entrypoint: "bash"
    args:
      - "-xe"
      - "-c"
      - |
        terraform init
        terraform plan -out terraform.tfplan
        terraform apply -input=false terraform.tfplan
secrets:
- kmsKeyName: projects/GOOGLE_PROJECT/locations/global/keyRings/aws-creds/cryptoKeys/aws-access-id
  secretEnv:
    AWS_ACCESS_KEY_ID: 'AWS_ACCESS_KEY_ID_ENCRYPTED_PASS'
- kmsKeyName: projects/GOOGLE_PROJECT/locations/global/keyRings/aws-creds/cryptoKeys/aws-secret-access-key
  secretEnv:
    AWS_SECRET_ACCESS_KEY: 'AWS_SECRET_ACCESS_KEY_ENCRYPTED_PASS'
