output "name" { value = "${module.gke.name}" }
output "location" { value = "${module.gke.location}" }
output "endpoint" { value = "${module.gke.endpoint}" }
