variable "project_id" {}
variable "path" { default = "hubgsa/gke_hub_sa_key.json" }
variable "gke_hub_sa_key_file" { default = "gke_hub_sa_key.json" }
variable "repo_url" {}

variable "vpc_id" {
    type = string
    description = "AWS VPC ID"
}

variable "eks_cluster_name" {
    type = string
    description = "EKS cluster name"
}

variable "private_subnets" {}

variable "env" {
    type = string
    description = "environment"
}

variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = list(string)

  default = [
    "777777777777",
    "888888888888",
  ]
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      rolearn  = "arn:aws:iam::66666666666:role/role1"
      username = "role1"
      groups   = ["system:masters"]
    },
  ]
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::66666666666:user/user1"
      username = "user1"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::66666666666:user/user2"
      username = "user2"
      groups   = ["system:masters"]
    },
  ]
}
